package com.project.web.exceptionHandler;

import com.project.web.exception.AllPlacesBookedException;
import com.project.web.exception.ConferenceNotFoundEception;
import com.project.web.exception.ConferenceRoomNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@SuppressWarnings("unused")
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConferenceRoomNotFoundException.class)
    protected ResponseEntity<String> handleConferenceRoomNotFound(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(ConferenceNotFoundEception.class)
    protected ResponseEntity<String> handleConferenceNotFound(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(AllPlacesBookedException.class)
    protected ResponseEntity<String> handleAllPlacesBooked(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }
}
