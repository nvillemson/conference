package com.project.web.controller;

import com.project.web.pojo.request.AddConferenceRoomRequest;
import com.project.web.pojo.response.ConferenceRoomBookingsResponse;
import com.project.web.pojo.response.ConferenceRoomsResponse;
import com.project.web.pojo.response.IdResponse;
import com.project.web.service.ConferenceRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@SuppressWarnings("unused")
@Controller
public class ConferenceRoomController {

    private ConferenceRoomService conferenceRoomService;

    @Autowired
    public ConferenceRoomController(ConferenceRoomService conferenceRoomService) {
        this.conferenceRoomService = conferenceRoomService;
    }

    @PostMapping("/add-conference-room")
    public @ResponseBody IdResponse addConferenceRoom(@RequestBody @Valid AddConferenceRoomRequest addConferenceRoomRequest) {
        return conferenceRoomService.addConferenceRoom(addConferenceRoomRequest);
    }

    @GetMapping("/get-conference-room-data")
    public @ResponseBody ConferenceRoomBookingsResponse searchConferenceRoomByName(@RequestParam("conferenceRoomId") String conferenceRoomId) {
        return conferenceRoomService.getConferenceRoomData(conferenceRoomId);
    }

    @GetMapping("/list-conference-rooms")
    public @ResponseBody ConferenceRoomsResponse listConferenceRoome() {
        return conferenceRoomService.listConferenceRooms();
    }

}
