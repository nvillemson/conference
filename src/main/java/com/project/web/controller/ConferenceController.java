package com.project.web.controller;

import com.project.web.pojo.request.AddConferenceRequest;
import com.project.web.pojo.request.AddParticipantRequest;
import com.project.web.pojo.response.ActiveConferencesResponse;
import com.project.web.pojo.response.ConferenceDataResponse;
import com.project.web.pojo.response.IdResponse;
import com.project.web.service.ConferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@SuppressWarnings("unused")
@Controller
public class ConferenceController {

    private ConferenceService conferenceService;

    @Autowired
    public ConferenceController(ConferenceService conferenceService) {
        this.conferenceService = conferenceService;
    }

    @PostMapping("/add-conference")
    public @ResponseBody IdResponse addConference(@RequestBody @Valid AddConferenceRequest addConferenceRequest) {
        return conferenceService.addConference(addConferenceRequest);
    }

    @PostMapping("/cancel-conference")
    @ResponseStatus(HttpStatus.OK)
    public void cancelConference(@RequestParam("conferenceId") long conferenceId) {
        conferenceService.cancelConference(conferenceId);
    }

    @GetMapping("/list-active-conferences")
    public @ResponseBody ActiveConferencesResponse listActiveConferences() {
        return conferenceService.listActiveConferences();
    }

    @GetMapping("/get-conference-data")
    public @ResponseBody ConferenceDataResponse getConferenceData(@RequestParam("conferenceId") long conferenceId) {
        return conferenceService.getConferenceData(conferenceId);
    }

    @PostMapping("/add-participant")
    public @ResponseBody IdResponse addConferenceParticipant(@RequestBody @Valid AddParticipantRequest addParticipantRequest) {
        return conferenceService.addConferenceParticipant(addParticipantRequest);
    }

    @PostMapping("/cancel-participant")
    @ResponseStatus(HttpStatus.OK)
    public void cancelConferenceParticipant(@RequestParam("conferenceId") long conferenceId, @RequestParam("participantId") long participantId) {
        conferenceService.cancelConferenceParticipant(conferenceId, participantId);
    }

}
