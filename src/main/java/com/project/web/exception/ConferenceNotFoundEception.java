package com.project.web.exception;

public class ConferenceNotFoundEception extends RuntimeException {

    public ConferenceNotFoundEception(String message) {
        super(message);
    }
}
