package com.project.web.exception;

public class ConferenceRoomNotFoundException extends RuntimeException {

    public ConferenceRoomNotFoundException(String message) {
        super(message);
    }

}
