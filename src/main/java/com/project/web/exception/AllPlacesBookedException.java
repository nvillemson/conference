package com.project.web.exception;

public class AllPlacesBookedException extends RuntimeException {

    public AllPlacesBookedException(String message) {
        super(message);
    }
}
