package com.project.web.pojo.request;

import javax.validation.constraints.NotNull;

public class AddConferenceRoomRequest {

    @NotNull
    private String conferenceRoomName;

    @NotNull
    private String conferenceRoomLocation;

    @NotNull
    private int maxSeats;

    public AddConferenceRoomRequest(@NotNull String conferenceRoomName, @NotNull String conferenceRoomLocation, @NotNull int maxSeats) {
        this.conferenceRoomName = conferenceRoomName;
        this.conferenceRoomLocation = conferenceRoomLocation;
        this.maxSeats = maxSeats;
    }

    public AddConferenceRoomRequest() {
    }

    public String getConferenceRoomName() {
        return conferenceRoomName;
    }

    public void setConferenceRoomName(String conferenceRoomName) {
        this.conferenceRoomName = conferenceRoomName;
    }

    public String getConferenceRoomLocation() {
        return conferenceRoomLocation;
    }

    public void setConferenceRoomLocation(String conferenceRoomLocation) {
        this.conferenceRoomLocation = conferenceRoomLocation;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }
}
