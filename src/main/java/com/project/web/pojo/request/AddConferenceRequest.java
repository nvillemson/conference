package com.project.web.pojo.request;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class AddConferenceRequest {

    @NotNull
    private long confereneRoomId;

    @NotNull
    private String conferenceName;

    @NotNull
    private LocalDateTime start;

    @NotNull
    private LocalDateTime end;

    public AddConferenceRequest(@NotNull long confereneRoomId, @NotNull String conferenceName, @NotNull LocalDateTime start, @NotNull LocalDateTime end) {
        this.confereneRoomId = confereneRoomId;
        this.conferenceName = conferenceName;
        this.start = start;
        this.end = end;
    }

    public AddConferenceRequest() {
    }

    public long getConfereneRoomId() {
        return confereneRoomId;
    }

    public void setConfereneRoomId(long confereneRoomId) {
        this.confereneRoomId = confereneRoomId;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }
}
