package com.project.web.pojo.request;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class AddParticipantRequest {

    @NotNull
    private long conferenceId;

    @NotNull
    private String fullName;

    @NotNull
    private LocalDateTime birthDate;

    public AddParticipantRequest(@NotNull long conferenceId, @NotNull String fullName, @NotNull LocalDateTime birthDate) {
        this.conferenceId = conferenceId;
        this.fullName = fullName;
        this.birthDate = birthDate;
    }

    public AddParticipantRequest() {
    }

    public long getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(long conferenceId) {
        this.conferenceId = conferenceId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }
}
