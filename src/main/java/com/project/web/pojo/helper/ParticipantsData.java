package com.project.web.pojo.helper;

import java.time.LocalDateTime;

public class ParticipantsData {

    private long id;
    private String fullName;
    private LocalDateTime birthDate;

    public ParticipantsData(long id, String fullName, LocalDateTime birthDate) {
        this.id = id;
        this.fullName = fullName;
        this.birthDate = birthDate;
    }

    public ParticipantsData() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }
}
