package com.project.web.pojo.helper;

import java.time.LocalDateTime;

public class ConferenceRoomData {

    private long id;
    private String conferenceRoomName;
    private String conferenceRoomLocation;
    private int conferenceRoomCapacity;
    private String conferenceName;
    private LocalDateTime conferenceStart;
    private LocalDateTime conferenceEnd;
    private long participantsCount;


    public ConferenceRoomData() {
    }

    public ConferenceRoomData(long id, String conferenceRoomName, String conferenceRoomLocation, int conferenceRoomCapacity,
                              String conferenceName, LocalDateTime conferenceStart, LocalDateTime conferenceEnd, long participantsCount) {
        this.id = id;
        this.conferenceRoomName = conferenceRoomName;
        this.conferenceRoomLocation = conferenceRoomLocation;
        this.conferenceRoomCapacity = conferenceRoomCapacity;
        this.conferenceName = conferenceName;
        this.conferenceStart = conferenceStart;
        this.conferenceEnd = conferenceEnd;
        this.participantsCount = participantsCount;
    }

    public String getConferenceRoomName() {
        return conferenceRoomName;
    }

    public void setConferenceRoomName(String conferenceRoomName) {
        this.conferenceRoomName = conferenceRoomName;
    }

    public String getConferenceRoomLocation() {
        return conferenceRoomLocation;
    }

    public void setConferenceRoomLocation(String conferenceRoomLocation) {
        this.conferenceRoomLocation = conferenceRoomLocation;
    }

    public int getConferenceRoomCapacity() {
        return conferenceRoomCapacity;
    }

    public void setConferenceRoomCapacity(int conferenceRoomCapacity) {
        this.conferenceRoomCapacity = conferenceRoomCapacity;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public LocalDateTime getConferenceStart() {
        return conferenceStart;
    }

    public void setConferenceStart(LocalDateTime conferenceStart) {
        this.conferenceStart = conferenceStart;
    }

    public LocalDateTime getConferenceEnd() {
        return conferenceEnd;
    }

    public void setConferenceEnd(LocalDateTime conferenceEnd) {
        this.conferenceEnd = conferenceEnd;
    }

    public long getParticipantsCount() {
        return participantsCount;
    }

    public void setParticipantsCount(long participantsCount) {
        this.participantsCount = participantsCount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
