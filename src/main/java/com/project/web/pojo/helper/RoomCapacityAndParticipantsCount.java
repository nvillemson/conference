package com.project.web.pojo.helper;

public class RoomCapacityAndParticipantsCount {

    private int conferenceRoomCapacity;
    private long participantCount;

    public RoomCapacityAndParticipantsCount(int conferenceRoomCapacity, long participantCount) {
        this.conferenceRoomCapacity = conferenceRoomCapacity;
        this.participantCount = participantCount;
    }

    public RoomCapacityAndParticipantsCount() {
    }

    public int getConferenceRoomCapacity() {
        return conferenceRoomCapacity;
    }

    public void setConferenceRoomCapacity(int conferenceRoomCapacity) {
        this.conferenceRoomCapacity = conferenceRoomCapacity;
    }

    public long getParticipantCount() {
        return participantCount;
    }

    public void setParticipantCount(long participantCount) {
        this.participantCount = participantCount;
    }
}
