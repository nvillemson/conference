package com.project.web.pojo.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

public class BookingResponse {

    private String conferenceName;

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime conferenceStart;

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime conferenceEnd;

    private long participantsCount;

    public BookingResponse(String conferenceName, LocalDateTime conferenceStart, LocalDateTime conferenceEnd, long participantsCount) {
        this.conferenceName = conferenceName;
        this.conferenceStart = conferenceStart;
        this.conferenceEnd = conferenceEnd;
        this.participantsCount = participantsCount;
    }

    public BookingResponse() {
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public LocalDateTime getConferenceStart() {
        return conferenceStart;
    }

    public void setConferenceStart(LocalDateTime conferenceStart) {
        this.conferenceStart = conferenceStart;
    }

    public LocalDateTime getConferenceEnd() {
        return conferenceEnd;
    }

    public void setConferenceEnd(LocalDateTime conferenceEnd) {
        this.conferenceEnd = conferenceEnd;
    }

    public long getParticipantsCount() {
        return participantsCount;
    }

    public void setParticipantsCount(long participantsCount) {
        this.participantsCount = participantsCount;
    }
}
