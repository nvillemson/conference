package com.project.web.pojo.response;

public class ConferenceRoomDataResponse {

    private long id;
    private String conferenceRoomName;
    private String conferenceRoomLocation;
    private int maxSeats;

    public ConferenceRoomDataResponse(long id, String conferenceRoomName, String conferenceRoomLocation, int maxSeats) {
        this.id = id;
        this.conferenceRoomName = conferenceRoomName;
        this.conferenceRoomLocation = conferenceRoomLocation;
        this.maxSeats = maxSeats;
    }

    public ConferenceRoomDataResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConferenceRoomName() {
        return conferenceRoomName;
    }

    public void setConferenceRoomName(String conferenceRoomName) {
        this.conferenceRoomName = conferenceRoomName;
    }

    public String getConferenceRoomLocation() {
        return conferenceRoomLocation;
    }

    public void setConferenceRoomLocation(String conferenceRoomLocation) {
        this.conferenceRoomLocation = conferenceRoomLocation;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeatsa) {
        this.maxSeats = maxSeats;
    }
}
