package com.project.web.pojo.response;

import java.util.List;

public class ConferenceRoomsResponse {

    private List<ConferenceRoomDataResponse> conferenceRooms;

    public ConferenceRoomsResponse(List<ConferenceRoomDataResponse> conferenceRooms) {
        this.conferenceRooms = conferenceRooms;
    }

    public ConferenceRoomsResponse() {
    }

    public List<ConferenceRoomDataResponse> getConferenceRooms() {
        return conferenceRooms;
    }

    public void setConferenceRooms(List<ConferenceRoomDataResponse> conferenceRooms) {
        this.conferenceRooms = conferenceRooms;
    }
}
