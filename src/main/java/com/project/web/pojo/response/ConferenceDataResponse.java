package com.project.web.pojo.response;

import com.project.web.pojo.helper.ParticipantsData;

import java.util.List;

public class ConferenceDataResponse {

    private ConferenceResponse conference;
    private List<ParticipantsData> participants;

    public ConferenceDataResponse(ConferenceResponse conference, List<ParticipantsData> participants) {
        this.conference = conference;
        this.participants = participants;
    }

    public ConferenceDataResponse() {
    }

    public List<ParticipantsData> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ParticipantsData> participants) {
        this.participants = participants;
    }

    public ConferenceResponse getConference() {
        return conference;
    }

    public void setConference(ConferenceResponse conference) {
        this.conference = conference;
    }
}
