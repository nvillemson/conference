package com.project.web.pojo.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

public class ActiveConferenceResponse {

    private long conferenceId;

    private String conferenceName;

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime conferenceStart;

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime conferenceEnd;

    private String conferenceRoomName;

    private String conferenceRoomLocation;

    private int conferenceRoomCapacity;

    private long participantCount;

    public ActiveConferenceResponse(long conferenceId, String conferenceName, LocalDateTime conferenceStart, LocalDateTime conferenceEnd,
                                    String conferenceRoomName, String conferenceRoomLocation, int conferenceRoomCapacity, long participantCount) {
        this.conferenceId = conferenceId;
        this.conferenceName = conferenceName;
        this.conferenceStart = conferenceStart;
        this.conferenceEnd = conferenceEnd;
        this.conferenceRoomName = conferenceRoomName;
        this.conferenceRoomLocation = conferenceRoomLocation;
        this.conferenceRoomCapacity = conferenceRoomCapacity;
        this.participantCount = participantCount;
    }

    public ActiveConferenceResponse() {
    }

    public long getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(long conferenceId) {
        this.conferenceId = conferenceId;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public LocalDateTime getConferenceStart() {
        return conferenceStart;
    }

    public void setConferenceStart(LocalDateTime conferenceStart) {
        this.conferenceStart = conferenceStart;
    }

    public LocalDateTime getConferenceEnd() {
        return conferenceEnd;
    }

    public void setConferenceEnd(LocalDateTime conferenceEnd) {
        this.conferenceEnd = conferenceEnd;
    }

    public String getConferenceRoomName() {
        return conferenceRoomName;
    }

    public void setConferenceRoomName(String conferenceRoomName) {
        this.conferenceRoomName = conferenceRoomName;
    }

    public String getConferenceRoomLocation() {
        return conferenceRoomLocation;
    }

    public void setConferenceRoomLocation(String conferenceRoomLocation) {
        this.conferenceRoomLocation = conferenceRoomLocation;
    }

    public int getConferenceRoomCapacity() {
        return conferenceRoomCapacity;
    }

    public void setConferenceRoomCapacity(int conferenceRoomCapacity) {
        this.conferenceRoomCapacity = conferenceRoomCapacity;
    }

    public long getParticipantCount() {
        return participantCount;
    }

    public void setParticipantCount(long participantCount) {
        this.participantCount = participantCount;
    }
}
