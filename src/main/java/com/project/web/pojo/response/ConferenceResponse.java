package com.project.web.pojo.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

public class ConferenceResponse {

    private long conferenceId;

    private String conferenceName;

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime start;

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime end;

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime cancelled;

    private String conferenceRoomName;

    private String conferenceRoomLocation;

    private int maxSeats;

    public ConferenceResponse(long conferenceId, String conferenceName, LocalDateTime start, LocalDateTime end, LocalDateTime cancelled,
                              String conferenceRoomName, String conferenceRoomLocation, int maxSeats) {
        this.conferenceId = conferenceId;
        this.conferenceName = conferenceName;
        this.start = start;
        this.end = end;
        this.cancelled = cancelled;
        this.conferenceRoomName = conferenceRoomName;
        this.conferenceRoomLocation = conferenceRoomLocation;
        this.maxSeats = maxSeats;
    }

    public ConferenceResponse() {
    }

    public long getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(long conferenceId) {
        this.conferenceId = conferenceId;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public LocalDateTime getCancelled() {
        return cancelled;
    }

    public void setCancelled(LocalDateTime cancelled) {
        this.cancelled = cancelled;
    }

    public String getConferenceRoomName() {
        return conferenceRoomName;
    }

    public void setConferenceRoomName(String conferenceRoomName) {
        this.conferenceRoomName = conferenceRoomName;
    }

    public String getConferenceRoomLocation() {
        return conferenceRoomLocation;
    }

    public void setConferenceRoomLocation(String conferenceRoomLocation) {
        this.conferenceRoomLocation = conferenceRoomLocation;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }
}
