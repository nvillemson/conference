package com.project.web.pojo.response;

import java.util.List;

public class ConferenceRoomBookingsResponse {

    private long conferenceRoomId;
    private String conferenceRoomName;
    private String conferenceRoomLocation;
    private int conferenceRoomCapacity;
    private List<BookingResponse> bookingResponses;


    public ConferenceRoomBookingsResponse() {
    }

    public ConferenceRoomBookingsResponse(long conferenceRoomId, String conferenceRoomName, String conferenceRoomLocation,
                                          int conferenceRoomCapacity, List<BookingResponse> bookingResponses) {
        this.conferenceRoomId = conferenceRoomId;
        this.conferenceRoomName = conferenceRoomName;
        this.conferenceRoomLocation = conferenceRoomLocation;
        this.conferenceRoomCapacity = conferenceRoomCapacity;
        this.bookingResponses = bookingResponses;
    }

    public String getConferenceRoomName() {
        return conferenceRoomName;
    }

    public void setConferenceRoomName(String conferenceRoomName) {
        this.conferenceRoomName = conferenceRoomName;
    }

    public String getConferenceRoomLocation() {
        return conferenceRoomLocation;
    }

    public void setConferenceRoomLocation(String conferenceRoomLocation) {
        this.conferenceRoomLocation = conferenceRoomLocation;
    }

    public int getConferenceRoomCapacity() {
        return conferenceRoomCapacity;
    }

    public void setConferenceRoomCapacity(int conferenceRoomCapacity) {
        this.conferenceRoomCapacity = conferenceRoomCapacity;
    }

    public List<BookingResponse> getBookingResponses() {
        return bookingResponses;
    }

    public void setBookingResponses(List<BookingResponse> bookingResponses) {
        this.bookingResponses = bookingResponses;
    }

    public long getConferenceRoomId() {
        return conferenceRoomId;
    }

    public void setConferenceRoomId(long conferenceRoomId) {
        this.conferenceRoomId = conferenceRoomId;
    }
}
