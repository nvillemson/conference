package com.project.web.pojo.response;

public class IdResponse {

    private long id;

    public IdResponse(long id) {
        this.id = id;
    }

    public IdResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
