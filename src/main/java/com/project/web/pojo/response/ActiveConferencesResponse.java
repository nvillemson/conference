package com.project.web.pojo.response;

import java.util.List;

public class ActiveConferencesResponse {

    private List<ActiveConferenceResponse> conferences;

    public ActiveConferencesResponse(List<ActiveConferenceResponse> conferences) {
        this.conferences = conferences;
    }

    public ActiveConferencesResponse() {
    }

    public List<ActiveConferenceResponse> getConferences() {
        return conferences;
    }

    public void setConferences(List<ActiveConferenceResponse> conferences) {
        this.conferences = conferences;
    }
}
