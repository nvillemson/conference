package com.project.web.service;

import com.project.persistence.model.ConferenceRoom;
import com.project.persistence.repository.ConferenceRoomRepository;
import com.project.web.exception.ConferenceRoomNotFoundException;
import com.project.web.pojo.helper.ConferenceRoomData;
import com.project.web.pojo.request.AddConferenceRoomRequest;
import com.project.web.pojo.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConferenceRoomService {

    private ConferenceRoomRepository conferenceRoomRepository;

    @Autowired
    public ConferenceRoomService(ConferenceRoomRepository conferenceRoomRepository) {
        this.conferenceRoomRepository = conferenceRoomRepository;
    }

    public IdResponse addConferenceRoom(AddConferenceRoomRequest req) {
        return new IdResponse(conferenceRoomRepository.saveConference(req.getConferenceRoomName(), req.getConferenceRoomLocation(), req.getMaxSeats()));
    }

    public ConferenceRoomBookingsResponse getConferenceRoomData(String conferenceRoomId) {
        List<ConferenceRoomData> conferenceRoomData = conferenceRoomRepository.searchConferenceRoomDataById(conferenceRoomId);
        if (conferenceRoomData.isEmpty()) {
            throw new ConferenceRoomNotFoundException("There is no such conference room: " + conferenceRoomId + ".");
        }

        List<BookingResponse> bookingResponses = new ArrayList<>();
        conferenceRoomData.forEach(singleBooking -> {
            BookingResponse bookingResponse = new BookingResponse(singleBooking.getConferenceName(), singleBooking.getConferenceStart(),
                    singleBooking.getConferenceEnd(), singleBooking.getParticipantsCount());
            bookingResponses.add(bookingResponse);
        });

        ConferenceRoomData firstBookingFound = conferenceRoomData.get(0);
        return new ConferenceRoomBookingsResponse(firstBookingFound.getId(), firstBookingFound.getConferenceRoomName(),
                firstBookingFound.getConferenceRoomLocation(), firstBookingFound.getConferenceRoomCapacity(), bookingResponses);
    }

    public ConferenceRoomsResponse listConferenceRooms() {
        Iterable<ConferenceRoom> all = conferenceRoomRepository.findAllRooms();
        List<ConferenceRoomDataResponse> conferenceRooms = new ArrayList<>();
        all.forEach(data -> {
            ConferenceRoomDataResponse conferenceRoom = new ConferenceRoomDataResponse(data.getId(), data.getConferenceRoomName(),
                    data.getConferenceRoomLocation(), data.getMaxSeats());
            conferenceRooms.add(conferenceRoom);
        });
        return new ConferenceRoomsResponse(conferenceRooms);
    }
}
