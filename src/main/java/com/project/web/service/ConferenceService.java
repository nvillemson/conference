package com.project.web.service;

import com.project.persistence.repository.ConferenceRepository;
import com.project.web.exception.AllPlacesBookedException;
import com.project.web.exception.ConferenceNotFoundEception;
import com.project.web.exception.ConferenceRoomNotFoundException;
import com.project.web.pojo.helper.ParticipantsData;
import com.project.web.pojo.helper.RoomCapacityAndParticipantsCount;
import com.project.web.pojo.request.AddParticipantRequest;
import com.project.web.pojo.response.*;
import com.project.web.pojo.request.AddConferenceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConferenceService {

    private ConferenceRepository conferenceRepository;

    @Autowired
    public ConferenceService(ConferenceRepository conferenceRepository) {
        this.conferenceRepository = conferenceRepository;
    }

    public IdResponse addConference(AddConferenceRequest req) {
        if(!conferenceRepository.findAvailableConferenceRoom(req.getConfereneRoomId(), req.getStart(), req.getEnd())) {
            throw new ConferenceRoomNotFoundException("Conference room unavailable or not found.");
        }
        return new IdResponse(conferenceRepository.addConference(req.getConferenceName(), req.getStart(), req.getEnd(), req.getConfereneRoomId()));
    }

    public void cancelConference(Long conferenceId) {
        conferenceRepository.cancelConference(conferenceId);
    }

    public ActiveConferencesResponse listActiveConferences() {
        return new ActiveConferencesResponse(conferenceRepository.listActiveConferences());
    }

    public ConferenceDataResponse getConferenceData(Long conferenceId) {
        try {
            ConferenceResponse conferenceData = conferenceRepository.getConferenceData(conferenceId);
            List<ParticipantsData> participants = conferenceRepository.getConferenceParticipants(conferenceId);
            return new ConferenceDataResponse(conferenceData, participants);
        } catch (EmptyResultDataAccessException ex) {
            throw new ConferenceNotFoundEception("There is no such conference with id: " + conferenceId + ".");
        }
    }

    public IdResponse addConferenceParticipant(AddParticipantRequest req) {
        try {
            RoomCapacityAndParticipantsCount capacity = conferenceRepository.getRoomCapacityAndParticipantCount(req.getConferenceId());
            if (capacity.getConferenceRoomCapacity() == capacity.getParticipantCount()) {
                throw new AllPlacesBookedException("Conference with id " + req.getConferenceId() + "is full.");
            }
            return new IdResponse(conferenceRepository.addParticipant(req.getFullName(), req.getBirthDate(), req.getConferenceId()));
        } catch (EmptyResultDataAccessException ex) {
            throw new ConferenceNotFoundEception("There is no such conference with id: " + req.getConferenceId() + ".");
        }
    }

    public void cancelConferenceParticipant(long conferenceId, long participantId) {
        conferenceRepository.cancelConferenceParticipant(conferenceId, participantId);
    }
}
