package com.project.persistence.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CONFERENCE_ROOM")
public class ConferenceRoom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String conferenceRoomName;

    @Column(name = "LOCATION", nullable = false)
    private String conferenceRoomLocation;

    @Column(name = "MAX_SEATS", nullable = false)
    private int maxSeats;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="conferenceRoom")
    private List<Conference> conferences;

    public ConferenceRoom(String conferenceRoomName, String conferenceRoomLocation, int maxSeats) {
        this.conferenceRoomName = conferenceRoomName;
        this.conferenceRoomLocation = conferenceRoomLocation;
        this.maxSeats = maxSeats;
    }

    public ConferenceRoom() {
    }

    public ConferenceRoom(Long id, String conferenceRoomName, String conferenceRoomLocation, int maxSeats) {
        this.id = id;
        this.conferenceRoomName = conferenceRoomName;
        this.conferenceRoomLocation = conferenceRoomLocation;
        this.maxSeats = maxSeats;
    }



    public String getConferenceRoomName() {
        return conferenceRoomName;
    }

    public void setConferenceRoomName(String conferenceRoomName) {
        this.conferenceRoomName = conferenceRoomName;
    }

    public String getConferenceRoomLocation() {
        return conferenceRoomLocation;
    }

    public void setConferenceRoomLocation(String conferenceRoomLocation) {
        this.conferenceRoomLocation = conferenceRoomLocation;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    public List<Conference> getConferences() {
        return conferences;
    }

    public void setConferences(List<Conference> conferences) {
        this.conferences = conferences;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
