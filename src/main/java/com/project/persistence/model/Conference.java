package com.project.persistence.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "CONFERENCE")
public class Conference {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String conferenceName;

    @Column(name = "START", nullable = false)
    private LocalDateTime start;

    @Column(name = "END", nullable = false)
    private LocalDateTime end;

    @Column(name = "CANCELLED")
    private LocalDateTime cancelled;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="CONFERENCE_ROOM_ID")
    public ConferenceRoom conferenceRoom;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="conference")
    public List<Participant> participants;

    public Conference() {
    }

    public Conference(Long id, String conferenceName, LocalDateTime start, LocalDateTime end) {
        this.id = id;
        this.conferenceName = conferenceName;
        this.start = start;
        this.end = end;
    }


    public String getConferenceName() {
        return conferenceName;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public LocalDateTime getCancelled() {
        return cancelled;
    }

    public void setCancelled(LocalDateTime cancelled) {
        this.cancelled = cancelled;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public ConferenceRoom getConferenceRoom() {
        return conferenceRoom;
    }

    public void setConferenceRoom(ConferenceRoom conferenceRoom) {
        this.conferenceRoom = conferenceRoom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
