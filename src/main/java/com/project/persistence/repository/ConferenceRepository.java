package com.project.persistence.repository;

import com.project.web.pojo.helper.RoomCapacityAndParticipantsCount;
import com.project.web.pojo.response.ConferenceResponse;
import com.project.web.pojo.response.ActiveConferenceResponse;
import com.project.web.pojo.helper.ParticipantsData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Repository
public class ConferenceRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ConferenceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional
    public Boolean findAvailableConferenceRoom(long conferenceRoomId, LocalDateTime start, LocalDateTime end) {
        return jdbcTemplate.queryForObject("SELECT EXISTS(SELECT cr.ID " +
                        "FROM CONFERENCE_ROOM cr LEFT JOIN CONFERENCE c ON (c.CONFERENCE_ROOM_ID = cr.ID AND c.CANCELLED IS NULL) " +
                        "WHERE (c.START NOT BETWEEN ? AND ? OR c.START IS NULL) AND (c.END NOT BETWEEN ? AND ? OR c.END IS NULL) AND cr.ID = ?)",
                new Object[]{Timestamp.valueOf(start), Timestamp.valueOf(end), Timestamp.valueOf(start), Timestamp.valueOf(end), conferenceRoomId}, Boolean.class);
    }

    @Transactional
    public void cancelConference(Long conferenceId) {
        jdbcTemplate.update("UPDATE CONFERENCE SET CANCELLED = NOW() WHERE ID = ? AND START > NOW()", conferenceId);
    }

    @Transactional
    public List<ActiveConferenceResponse> listActiveConferences() {
        return jdbcTemplate.query("SELECT c.ID AS id, c.NAME AS conferenceName, c.START AS conferenceStart, c.END AS conferenceEnd, " +
                "cr.NAME AS conferenceRoomName, cr.LOCATION AS conferenceRoomLocation, cr.MAX_SEATS AS conferenceRoomCapacity, COUNT(p.ID) as participantCount " +
                "FROM CONFERENCE c INNER JOIN CONFERENCE_ROOM cr ON (c.CONFERENCE_ROOM_ID = cr.ID AND c.CANCELLED IS NULL) " +
                "LEFT JOIN PARTICIPANT p ON (p.CONFERENCE_ID = c.ID AND p.CANCELLED IS NULL) " +
                "WHERE c.END > NOW() " +
                "GROUP BY c.ID ",
                (rs, rowNum) -> new ActiveConferenceResponse(rs.getLong("id"), rs.getString("conferenceName"),
                        rs.getTimestamp("conferenceStart").toLocalDateTime(), rs.getTimestamp("conferenceEnd").toLocalDateTime(),
                        rs.getString("conferenceRoomName"), rs.getString("conferenceRoomLocation"),
                        rs.getInt("conferenceRoomCapacity"), rs.getLong("participantCount")));
    }

    @Transactional
    public ConferenceResponse getConferenceData(Long conferenceId) {
        return jdbcTemplate.queryForObject("SELECT c.ID AS id, c.NAME AS conferenceName, c.START AS conferenceStart, c.END AS conferenceEnd, " +
                "c.CANCELLED AS cancelled, cr.NAME AS conferenceRoomName, cr.LOCATION AS conferenceRoomLocation, cr.MAX_SEATS AS conferenceRoomCapacity " +
                "FROM CONFERENCE c INNER JOIN CONFERENCE_ROOM cr ON c.CONFERENCE_ROOM_ID = cr.ID " +
                "WHERE c.ID = ?", new Object[] { conferenceId },
                (rs, rowNum) -> new ConferenceResponse(rs.getLong("id"), rs.getString("conferenceName"),
                        rs.getTimestamp("conferenceStart").toLocalDateTime(), rs.getTimestamp("conferenceEnd").toLocalDateTime(),
                        rs.getTimestamp("cancelled") != null ? rs.getTimestamp("cancelled").toLocalDateTime() : null,
                        rs.getString("conferenceRoomName"), rs.getString("conferenceRoomLocation"),
                        rs.getInt("conferenceRoomCapacity")));
    }

    @Transactional
    public List<ParticipantsData> getConferenceParticipants(Long conferenceId) {
        return jdbcTemplate.query("SELECT * FROM PARTICIPANT WHERE CONFERENCE_ID = ? AND CANCELLED IS NULL ", new Object[] { conferenceId },
                (rs, rowNum) -> new ParticipantsData(rs.getLong("ID"), rs.getString("NAME"),
                        rs.getTimestamp("BIRTH_DATE").toLocalDateTime()));
    }

    @Transactional
    public RoomCapacityAndParticipantsCount getRoomCapacityAndParticipantCount(long conferenceId) {
        return jdbcTemplate.queryForObject("SELECT cr.MAX_SEATS AS conferenceRoomCapacity, COUNT(p.ID) as participantCount  " +
                "FROM CONFERENCE c INNER JOIN CONFERENCE_ROOM cr ON (c.CONFERENCE_ROOM_ID = cr.ID AND c.CANCELLED IS NULL) " +
                "LEFT JOIN PARTICIPANT p ON (p.CONFERENCE_ID = c.ID AND p.CANCELLED IS NULL) " +
                "WHERE c.ID = ? " +
                "GROUP BY c.ID", new Object[] { conferenceId },
                (rs, rowNum) -> new RoomCapacityAndParticipantsCount(rs.getInt("conferenceRoomCapacity"), rs.getLong("participantCount")));
    }

    @Transactional
    public void cancelConferenceParticipant(long conferenceId, long participantId) {
        jdbcTemplate.update("UPDATE PARTICIPANT SET CANCELLED = NOW() WHERE CONFERENCE_ID = ? AND ID = ? ", conferenceId, participantId);
    }

    @Transactional
    public Long addConference(String conferenceName, LocalDateTime start, LocalDateTime end, long conferenceRoomId) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO CONFERENCE(NAME, START, END, CONFERENCE_ROOM_ID) VALUES (?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, conferenceName);
            ps.setTimestamp(2, Timestamp.valueOf(start));
            ps.setTimestamp(3, Timestamp.valueOf(end));
            ps.setLong(4, conferenceRoomId);
            return ps; }, holder);
        return Objects.requireNonNull(holder.getKey()).longValue();
    }

    @Transactional
    public Long addParticipant(String fullName, LocalDateTime birthDate, long conferenceId) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO PARTICIPANT(NAME, BIRTH_DATE, CONFERENCE_ID) VALUES (?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, fullName);
            ps.setTimestamp(2, Timestamp.valueOf(birthDate));
            ps.setLong(3, conferenceId);
            return ps; }, holder);
        return Objects.requireNonNull(holder.getKey()).longValue();
    }
}
