package com.project.persistence.repository;

import com.project.persistence.model.ConferenceRoom;
import com.project.web.pojo.helper.ConferenceRoomData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;

@Repository
public class ConferenceRoomRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ConferenceRoomRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional
    public List<ConferenceRoom> findAllRooms() {
        return jdbcTemplate.query("SELECT * FROM CONFERENCE_ROOM",
                (rs, rowNum) -> new ConferenceRoom(rs.getLong("ID"), rs.getString("NAME"), rs.getString("LOCATION"),
                        rs.getInt("MAX_SEATS")));
    }

    @Transactional
    public List<ConferenceRoomData> searchConferenceRoomDataById(String conferenceRoomId) {
        return jdbcTemplate.query("SELECT cr.ID AS id, cr.NAME AS conferenceRoomName, cr.LOCATION AS conferenceRoomLocation, cr.MAX_SEATS AS conferenceRoomCapacity, " +
                        "c.NAME AS conferenceName, c.START AS conferenceStart, c.END AS conferenceEnd, COUNT(p.ID) AS participantsCount " +
                        "FROM CONFERENCE_ROOM cr LEFT JOIN CONFERENCE c ON (c.CONFERENCE_ROOM_ID = cr.ID AND c.CANCELLED IS NULL) " +
                        "LEFT JOIN PARTICIPANT p ON (p.CONFERENCE_ID = c.ID AND p.CANCELLED IS NULL)  " +
                        "WHERE cr.ID = ? " +
                        "GROUP BY c.ID", new Object[] { conferenceRoomId },
                (rs, rowNum) -> new ConferenceRoomData(rs.getLong("id"), rs.getString("conferenceRoomName"),
                        rs.getString("conferenceRoomLocation"), rs.getInt("conferenceRoomCapacity"), rs.getString("conferenceName"),
                        rs.getTimestamp("conferenceStart").toLocalDateTime(), rs.getTimestamp("conferenceEnd").toLocalDateTime(),
                        rs.getLong("participantsCount")));
    }

    @Transactional
    public long saveConference(String name, String location, int maxSeats ) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO CONFERENCE_ROOM(NAME, LOCATION, MAX_SEATS) VALUES (?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, name);
            ps.setString(2, location);
            ps.setInt(3, maxSeats);
            return ps; }, holder);
        return Objects.requireNonNull(holder.getKey()).longValue();
    }
}
