package tests;

import com.project.web.pojo.request.AddConferenceRequest;
import com.project.web.pojo.request.AddConferenceRoomRequest;
import com.project.web.pojo.request.AddParticipantRequest;
import general.AbstractTest;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import java.time.LocalDateTime;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.IsEqual.equalTo;

public class ConferenceRoomControllerTest extends AbstractTest {

    @Test
    public void testAddConferenceRoomBadRequest() {
        given()
                .body(new AddConferenceRoomRequest())
                .contentType(ContentType.JSON)
                .when().
                post(addConferenceRoomUrl).
                then().
                statusCode(HttpStatus.SC_BAD_REQUEST);

        given()
                .body(new AddConferenceRoomRequest("Test", null, 150))
                .contentType(ContentType.JSON)
                .when().
                post(addConferenceRoomUrl).
                then().
                statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testAddConferenceRoom() {
        when().
                get(listConferenceRoomUrl).
                then().
                statusCode(HttpStatus.SC_OK).body("conferenceRooms", empty());

        addConferenceRoom(new AddConferenceRoomRequest("TestName1", "Solaris", 150));

        when().
                get(listConferenceRoomUrl).
                then().
                statusCode(HttpStatus.SC_OK)
                .body("conferenceRooms.size()", equalTo(1))
                .body("conferenceRooms[0].conferenceRoomName", equalTo("TestName1"))
                .body("conferenceRooms[0].conferenceRoomLocation", equalTo("Solaris"))
                .body("conferenceRooms[0].maxSeats", equalTo(150));

        addConferenceRoom(new AddConferenceRoomRequest("TestName2", "Viru Hotel", 100));

        when().
                get(listConferenceRoomUrl).
                then().
                statusCode(HttpStatus.SC_OK)
                .body("conferenceRooms.size()", equalTo(2))
                .body("conferenceRooms[0].conferenceRoomName", equalTo("TestName1"))
                .body("conferenceRooms[0].conferenceRoomLocation", equalTo("Solaris"))
                .body("conferenceRooms[0].maxSeats", equalTo(150))
                .body("conferenceRooms[1].conferenceRoomName", equalTo("TestName2"))
                .body("conferenceRooms[1].conferenceRoomLocation", equalTo("Viru Hotel"))
                .body("conferenceRooms[1].maxSeats", equalTo(100));
    }

    @Test
    public void testSearchConferenceRoom() {
        // NO SUCH CONFERENCE
        given()
                .get(getConferenceRoomUrl + "?conferenceRoomId=1")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);

        long conferenceRoomId = addConferenceRoom(new AddConferenceRoomRequest("Test", "Solaris", 10));

        LocalDateTime start = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).plusHours(2);
        LocalDateTime end = start.plusHours(3);

        long conferenceId1 = addConference(new AddConferenceRequest(conferenceRoomId, "Java Conference", start, end));

        start = end.plusHours(3);
        end = start.plusHours(3);
        long conferenceId2 = addConference(new AddConferenceRequest(conferenceRoomId, "Kotlin Conference", start, end));

        LocalDateTime participant1Birthday = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).minusYears(25);
        addParticipant(new AddParticipantRequest(conferenceId1, "John", participant1Birthday));

        LocalDateTime participant2Birthday = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).minusYears(23);
        addParticipant(new AddParticipantRequest(conferenceId1, "Peter", participant2Birthday));

        given()
                .get(getConferenceRoomUrl + "?conferenceRoomId=" + conferenceRoomId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("conferenceRoomId", equalTo((int) conferenceRoomId))
                .body("conferenceRoomName", equalTo("Test"))
                .body("conferenceRoomLocation", equalTo("Solaris"))
                .body("conferenceRoomCapacity", equalTo(10))
                .body("bookingResponses.size()", equalTo(2))
                .body("bookingResponses[0].conferenceName", equalTo("Java Conference"))
                .body("bookingResponses[0].participantsCount", equalTo(2))
                .body("bookingResponses[1].conferenceName", equalTo("Kotlin Conference"))
                .body("bookingResponses[1].participantsCount", equalTo(0));

        cancelConference(conferenceId2);

        given()
                .get(getConferenceRoomUrl + "?conferenceRoomId=" + conferenceRoomId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("conferenceRoomId", equalTo((int) conferenceRoomId))
                .body("conferenceRoomName", equalTo("Test"))
                .body("conferenceRoomLocation", equalTo("Solaris"))
                .body("conferenceRoomCapacity", equalTo(10))
                .body("bookingResponses.size()", equalTo(1))
                .body("bookingResponses[0].conferenceName", equalTo("Java Conference"))
                .body("bookingResponses[0].participantsCount", equalTo(2));
    }
}
