package tests;

import com.project.web.pojo.request.AddConferenceRequest;
import com.project.web.pojo.request.AddConferenceRoomRequest;
import com.project.web.pojo.request.AddParticipantRequest;
import general.AbstractTest;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import java.time.LocalDateTime;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ConferenceControllerTest extends AbstractTest {

    @Test
    public void testAddCancelConference() {
        Long conferenceRoomId = addConferenceRoom(new AddConferenceRoomRequest("TestName1", "Solaris", 150));

        LocalDateTime start = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).plusHours(2);
        LocalDateTime end = start.plusHours(3);
        Long conferenceId = addConference(new AddConferenceRequest(conferenceRoomId, "Java Conference", start, end));

        given()
                .get(listConferenceUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("conferences.size()", equalTo(1))
                .body("conferences[0].conferenceId", equalTo(conferenceId.intValue()))
                .body("conferences[0].conferenceName", equalTo("Java Conference"))
                .body("conferences[0].conferenceRoomName", equalTo("TestName1"))
                .body("conferences[0].conferenceRoomLocation", equalTo("Solaris"))
                .body("conferences[0].conferenceRoomCapacity", equalTo(150))
                .body("conferences[0].participantCount", equalTo(0));

        String cancelled = given()
                .get(getConferenceDataUrl + "?conferenceId=" + conferenceId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().jsonPath().getString("conference.cancelled");
        assertNull(cancelled);

        cancelConference(conferenceId);

        cancelled = given()
                .get(getConferenceDataUrl + "?conferenceId=" + conferenceId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().jsonPath().getString("conference.cancelled");
        assertNotNull(cancelled);

        given()
                .get(listConferenceUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("conferences.size()", equalTo(0));
    }

    @Test
    public void testAddConferenceRoomBookedBadRequest() {
        long conferenceRoomId = addConferenceRoom(new AddConferenceRoomRequest("TestName1", "Solaris", 150));

        LocalDateTime start = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).plusHours(2);
        LocalDateTime end = start.plusHours(3);
        addConference(new AddConferenceRequest(conferenceRoomId, "Java Conference", start, end));

        // room booked
        start = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).plusHours(1);
        end = start.plusHours(2);
        given()
                .body(new AddConferenceRequest(conferenceRoomId, "Scala Conference2", start, end))
                .contentType(ContentType.JSON)
                .when()
                .post(addConferenceUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testAddCancelParticipant() {
        Long conferenceRoomId = addConferenceRoom(new AddConferenceRoomRequest("TestName1", "Solaris", 75));

        LocalDateTime start = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).plusHours(2);
        LocalDateTime end = start.plusHours(3);
        long conferenceId = addConference(new AddConferenceRequest(conferenceRoomId, "Java Conference", start, end));

        LocalDateTime participant1Birthday = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).minusYears(25);
        long participantId1 = addParticipant(new AddParticipantRequest(conferenceId, "John", participant1Birthday));

        LocalDateTime participant2Birthday = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).minusYears(23);
        long participantId2 = addParticipant(new AddParticipantRequest(conferenceId, "Peter", participant2Birthday));

        given()
                .get(listConferenceUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("conferences.size()", equalTo(1))
                .body("conferences[0].conferenceId", equalTo((int) conferenceId))
                .body("conferences[0].participantCount", equalTo(2));

        // cancel participant
        cancelParticipant(conferenceId, participantId2);

        given()
                .get(listConferenceUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("conferences.size()", equalTo(1))
                .body("conferences[0].conferenceId", equalTo((int) conferenceId))
                .body("conferences[0].participantCount", equalTo(1));

        given()
                .get(getConferenceDataUrl + "?conferenceId=" + conferenceId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("conference.maxSeats", equalTo(75))
                .body("conference.conferenceName", equalTo("Java Conference"))
                .body("conference.conferenceRoomName", equalTo("TestName1"))
                .body("conference.conferenceRoomLocation", equalTo("Solaris"))
                .body("participants.size()", equalTo(1))
                .body("participants[0].id", equalTo((int) participantId1))
                .body("participants[0].fullName", equalTo("John"));
    }

    @Test
    public void testAddParticipantConferenceCancelledBadRequest() {
        long conferenceRoomId = addConferenceRoom(new AddConferenceRoomRequest("TestName1", "Solaris", 150));

        LocalDateTime start = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).plusHours(2);
        LocalDateTime end = start.plusHours(3);

        long conferenceId = addConference(new AddConferenceRequest(conferenceRoomId, "Java Conference", start, end));

        cancelConference(conferenceId);

        LocalDateTime participant1Birthday = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).minusYears(25);
        given()
                .body(new AddParticipantRequest(conferenceRoomId, "John", participant1Birthday))
                .contentType(ContentType.JSON)
                .when()
                .post(addParticipantUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testAddParticipantRoomFullBadRequest() {
        long conferenceRoomId = addConferenceRoom(new AddConferenceRoomRequest("TestName1", "Solaris", 3));

        LocalDateTime start = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).plusHours(2);
        LocalDateTime end = start.plusHours(3);

        long conferenceId = addConference(new AddConferenceRequest(conferenceRoomId, "Java Conference", start, end));

        LocalDateTime participant1Birthday = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0).minusYears(25);
        for (int i = 0; i < 3; i++) {
            addParticipant(new AddParticipantRequest(conferenceId, "John", participant1Birthday));
        }

        // room full
        given()
                .body(new AddParticipantRequest(conferenceId, "John", participant1Birthday))
                .contentType(ContentType.JSON)
                .when()
                .post(addParticipantUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }
}
