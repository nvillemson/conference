package general;

import com.project.Application;
import com.project.persistence.repository.BaseTestRepository;
import com.project.web.pojo.request.AddConferenceRequest;
import com.project.web.pojo.request.AddConferenceRoomRequest;
import com.project.web.pojo.request.AddParticipantRequest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class AbstractTest {

    protected final String addConferenceRoomUrl = "/add-conference-room";
    protected final String listConferenceRoomUrl = "/list-conference-rooms";
    protected final String getConferenceRoomUrl = "/get-conference-room-data";

    protected final String addConferenceUrl = "/add-conference";
    protected final String addParticipantUrl = "/add-participant";
    protected final String cancelParticipantUrl = "/cancel-participant";
    protected final String cancelConferenceUrl = "/cancel-conference";
    protected final String getConferenceDataUrl = "/get-conference-data";
    protected final String listConferenceUrl = "/list-active-conferences";

    @Value("${server.port}")
    private int serverPort;

    @Autowired
    private BaseTestRepository baseTestRepository;

    @Before
    public void setUp() {
        baseTestRepository.clearDatabase();
        RestAssured.port = serverPort;
    }

    protected long addParticipant(AddParticipantRequest request){
        return given()
                .body(request)
                .contentType(ContentType.JSON)
                .when()
                .post(addParticipantUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().jsonPath().getLong("id");
    }

    protected long addConference(AddConferenceRequest request) {
        return given()
                .body(request)
                .contentType(ContentType.JSON)
                .when()
                .post(addConferenceUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().jsonPath().getLong("id");
    }

    protected long addConferenceRoom(AddConferenceRoomRequest request) {
        return given()
                .body(request)
                .contentType(ContentType.JSON)
                .when()
                .post(addConferenceRoomUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().jsonPath().getLong("id");
    }

    protected void cancelParticipant(long conferenceId, long participantId) {
        given()
                .post(cancelParticipantUrl + "?conferenceId=" + conferenceId + "&participantId=" + participantId)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    protected void cancelConference(long conferenceId) {
        given()
                .post(cancelConferenceUrl + "?conferenceId=" + conferenceId)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }
}
